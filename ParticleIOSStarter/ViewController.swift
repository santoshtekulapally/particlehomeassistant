//
//  ViewController.swift
//  ParticleIOSStarter
//
//  Created by Parrot on 2019-06-29.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import Particle_SDK
import Speech


class ViewController: UIViewController, SFSpeechRecognizerDelegate{
    
    
    
    let audioEngine = AVAudioEngine()
    let speechRecognizer: SFSpeechRecognizer? = SFSpeechRecognizer()
    let request = SFSpeechAudioBufferRecognitionRequest()
    var recognitionTask: SFSpeechRecognitionTask?
    var isRecording = false
    var catchstring  : String?
    
    var bestString:String!
    
    

    
    @IBOutlet weak var startstop: UIButton!
    @IBOutlet weak var voicetextlbl: UILabel!

    @IBOutlet weak var colournamelbl: UILabel!
    @IBAction func startstoppressed(_ sender: Any) {
        
      
        
        self.voicetextlbl.text = ""
        self.catchstring = ""
        
        
        if isRecording == true {
            
            cancelRecording()
            isRecording = false
            self.startstop.backgroundColor = UIColor.gray
            startstop.setTitle("Press here to speak", for: .normal)
            self.bestString = " "
            
        } else {
            
            self.recordAndRecognizeSpeech()
            isRecording = true
            self.startstop.backgroundColor = UIColor.red
            startstop.setTitle("Speak , I am listening", for: .normal)
            
        }
        
        
        
    }
    
    
    // MARK: User variables
    let USERNAME = "santoshonthemove@gmail.com"
    let PASSWORD = "santosh@123"
    
    // MARK: Device
    let DEVICE_ID = "25001a000f47363333343437"
    var myPhoton : ParticleDevice?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.requestSpeechAuthorization()


        // 1. Initialize the SDK
        ParticleCloud.init()
 
        
        // initializing all the code for connecting the particle to the simulator
        
        
        
        // 2. Login to your account
        ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
            if (error != nil) {
                // Something went wrong!
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                // Print out more detailed information
                print(error?.localizedDescription)
            }
            else {
                print("Login success!")
                self.getDeviceFromCloud()
            }
        } // end login
    }
    //MARK: - Colors
    enum Color: String {
        case Red, Orange, Yellow, Green, Blue, Purple, Black, Gray
        
        var create: UIColor {
            switch self {
            case .Red:
                return UIColor.red
            case .Orange:
                return UIColor.orange
            case .Yellow:
                return UIColor.yellow
            case .Green:
                return UIColor.green
            case .Blue:
                return UIColor.blue
            case .Purple:
                return UIColor.purple
            case .Black:
                return UIColor.black
            case .Gray:
                return UIColor.gray
            }
        }
    }
    
    func cancelRecording() {
        recognitionTask?.finish()
        recognitionTask = nil
        
        // stop audio
        request.endAudio()
        audioEngine.stop()
        audioEngine.inputNode.removeTap(onBus: 0)
    }
    
    //MARK: - Recognize Speech
    func recordAndRecognizeSpeech() {
        
        
        let node = audioEngine.inputNode
        let recordingFormat = node.outputFormat(forBus: 0)
        node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { buffer, _ in
            self.request.append(buffer)
        }
        audioEngine.prepare()
        do {
            try audioEngine.start()
        } catch {
            self.sendAlert(title: "Speech Recognizer Error", message: "There has been an audio engine error.")
            return print(error)
        }
        guard let myRecognizer = SFSpeechRecognizer() else {
            self.sendAlert(title: "Speech Recognizer Error", message: "Speech recognition is not supported for your current locale.")
            return
        }
        if !myRecognizer.isAvailable {
            self.sendAlert(title: "Speech Recognizer Error", message: "Speech recognition is not currently available. Check back at a later time.")
            // Recognizer is not available right now
            return
        }
        recognitionTask = speechRecognizer?.recognitionTask(with: request, resultHandler: { result, error in
            if let result = result {
                
                self.bestString = result.bestTranscription.formattedString
                var lastString: String = ""
                
                for segment in result.bestTranscription.segments {
                    let indexTo = self.bestString.index(self.bestString.startIndex, offsetBy: segment.substringRange.location)
                    lastString = String(self.bestString[indexTo...])
                    
                    self.voicetextlbl.text = self.bestString
                    
                    self.catchstring = self.bestString
                    
                    print(self.catchstring!)
                    
                    if(self.bestString == "Turn on") || (self.bestString == "turn on")
                    {
                        self.swtichonlights()
                        
                        
                    }
                    else
                        if(self.bestString == "Turn off") || (self.bestString == "turn off")
                        {
                            
                            self.swtichofflights()
                            
                            
                            
                        } else
                            if(self.bestString == "Rainbow") || (self.bestString == "rainbow")
                            {
                                
                                self.rainbowlights()
                                
                            } else
                                
                                if(self.bestString == "Go sign") || (self.bestString == "go sign")
                                {
                                    self.greenlights()
                                    
                                }else
                                    
                                    if(self.bestString == "Stop sign") || (self.bestString == "stop sign")
                                    {
                                        
                                        self.redlights()
                    }
                    
                }
                
                self.checkForColorsSaid(resultString: lastString)
            } else if let error = error {
                self.sendAlert(title: "Speech Recognizer Error", message: "There has been a speech recognition error.")
                print(error)
            }
        })
    }
    func showAlert(withTitle title: String, message: String) {
        
        

        
        let alert = UIAlertController(title: "Please power on Your alexa", message: "Connection error", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
      
        
        
        
        
        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: { action in
            
           
        
            
        }))
       
        
   //     view?.window?.rootViewController?.present(alert, animated: true)
        
        
     
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: Get Device from Cloud
    // Gets the device from the Particle Cloud
    // and sets the global device variable
    func getDeviceFromCloud() {
    ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
        
        
      
        
        
        
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            }
            else {
                print("Got photon: \(device?.id)")
                self.myPhoton = device
            }
            
        } // end getDevice()
    }

  
   
    func swtichonlights()  {
        
        let funarray = ["turnon"] as [Any]
        
        var newtask = myPhoton!.callFunction("turnon", withArguments: funarray) { (resultCode : NSNumber?, error : Error?) -> Void in
            
            if (error == nil) {
                
                
                
                print("LED on D7 successfully turned on")
            }
              

        
    }
    }
    func swtichofflights()  {
        
       
        let funcArgs = ["turnoff",1] as [Any]
        var task = myPhoton!.callFunction("turnoff", withArguments: funcArgs) { (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("LED on D7 successfully turned off")
             
            }
            else{
                
            }
        }
        
    }
    //MARK: - Recognize Speech



    
    //MARK: - UI / Set view color.
  
    
    //MARK: - Alert
    func sendAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    func requestSpeechAuthorization() {
        SFSpeechRecognizer.requestAuthorization { authStatus in
            OperationQueue.main.addOperation {
                switch authStatus {
                case .authorized:
                    self.startstop.isEnabled = true
                case .denied:
                    self.startstop.isEnabled = false
                    self.voicetextlbl.text = "User denied access to speech recognition"
                case .restricted:
                    self.startstop.isEnabled = false
                    self.voicetextlbl.text = "Speech recognition restricted on this device"
                case .notDetermined:
                    self.startstop.isEnabled = false
                    self.voicetextlbl.text = "Speech recognition not yet authorized"
                @unknown default:
                    return
                }
            }
        }
    }

    func rainbowlights()  {
        
        let funcArgs = ["Rainbow",1] as [Any]
        var task = myPhoton!.callFunction("Rainbow", withArguments: funcArgs) { (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                
                print("rainbow will play now")
            }
            else{
                
            }
        }
        
    }

    func greenlights()  {
        
        let funcArgs = ["greenSignal",1] as [Any]
        var task = myPhoton!.callFunction("greenSignal", withArguments: funcArgs) { (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                
                print("green lights")
                self.voicetextlbl.text = ""
            }
            else{
                
            }
        }
        
    }

    func redlights()  {
        
        let funcArgs = ["redSignal",1] as [Any]
        var task = myPhoton!.callFunction("redSignal", withArguments: funcArgs) { (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                
                print("red lights")
                self.voicetextlbl.text = ""
                
            }
            else{
                
            }
        }
        
    }


func checkForColorsSaid(resultString: String) {
    guard let color = Color(rawValue: resultString) else { return }
   // colourview.backgroundColor = color.create
    self.voicetextlbl.text = resultString
}

////MARK: - Alert
//func sendAlert(title: String, message: String) {
//    let alert = UIAlertController(title: title, message: "felt like sying", preferredStyle: UIAlertController.Style.alert)
//    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//    self.present(alert, animated: true, completion: nil)
//}

//MARK: - UI / Set view color.


   
//    var bytesToReceive : Int64 = task.countOfBytesExpectedToReceive
//
    

}
